# PROOF FORMAT FOR REAL-WORLD RESERVE ASSETS 

This is draft status


A collection of proofs represents contracts describing real and virtual assets posted as reserve for a specific token issuance.

The underlying resources for the proofs may be documents, recordings, photos, crypto transactions.

sha256sum is calculated on files. crypto transaction hashes are used as-is. All data is is left-padded to 512 bit vectors.

The hashes are ordered in sequence of their big-endian numerical value and sha256sum is calculated on the sequence. This is the collateral proof.

--

The proof should be signed by the key performing the initial reserve transaction and made available. The following options should be considered:

- A secp256k1 ECC signature on the proof
- A ethereum blockchain transaction to a well-known address, with the proof in the data field
- An ENS record

Additional signatures may optionally be added.


---

The proof, signature and underlyding digital resources must be disseminated to a selection of storers whose role is to provide data availability on demand.

With the eventual maturity of decentralized storage, additional data availability guarantees can come from these.

---

metadata structures can be added to provide metadata and context for the digial resources in the proof.

This structure can also allow alternate hash representations of the data, aswell as additional signatures.

Lastly it can include a list of locations where the resources can be obtained. The resources should be available on querying the proof hash, responding with the resources in a well-known achive format, and optionally providing a human readable index of the files (the client can choose using the 'Accept' HTTP header)

---

example:

A contract for a token reserve consists of a written document A, a video recording B and a crypto transaction C.

sha256(A), sha256(B) are calculated. They result in Ah = 0x56.., Bh = 0x12. C = 0x34..

sha256 is calculated on the ordered sequence to obtain the proof; sha256(Bh, C, Ah) = Ph 

Token T is minted by sending reserve from account K. Ph is signed by the same key; secp256k1.ec_sign(K, Ph) = Ks

The proof is encapsulated by metadata:

```
{
	'version': 0,
	'token': <token address>,
	'proofs': {
		'default': {
			'digest': <Ph>,
			'signatures': [
				<Ks>,
			],
		},
		'foo-algo': [
			'digest': <Ph2>,
			'signatures': [
				...,
			],
		],
	},
	'resources': [
		{
			'type': 'file',
			'proof': {
				'default': {
					'digest': <...>,
				}
			},
			'metadata': {
				'mime-type': 'application/pdf',
				'filename': 'foo.pdf',
			},
		},
		{
			'type': 'ethereum-transaction',
			'proof': {
				'default': {
					'digest': <...>,
				}
			},
		},
	],
	'locations': [
		'ftp://ftp.grassrootseconomics.org/reserve-resources',
		'https://redcross.com/grassrootseconomics/reserve-resources',
		...
	]
}
```

---

Another possiblity is to host the proofs on DNS TXT records by well-known actors. This means, for example, if _<Ph>_resource_proofs.grassrootsconomics.org could returns the token address (or the other way around), that means GE considers the proof valid for the token. A list of DNS hosts can be established to decentralize this certification.
