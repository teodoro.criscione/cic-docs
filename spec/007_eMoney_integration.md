## Notes ##
Users have no way to automate the excahnge of CIC <> National Currency. Can people be encouraged to be liquidity providers and develop pools containing CIC and National Currency or other token? Creating a on-chain liquidity pool still requires on and off-boarding crypto to fiat.


## Solution ##
Using AfricasTalking API to automatically send Mpesa from a business account when CIC is received (Blockchain account triggered):
Using RPC endpoints and contract calls to automatically send CIC when KSH is received (MPesa call back triggered)

A liquidity pool can be created off-chain where a liquidity provider simply monitors two (float) accounts one of CIC and another of Mpesa.

- CIC - > Mpesa 
   - CICs are added to the float account account triggering Mpesa to be released from the mpesa (float)
   - We send back an amount of Mpesa to the user from out MPESA float account (this amount can be based on a bonding curve)
- Mpesa -> CIC
   - Mpesa are added to our float Mpesa account.
   - CIC from our Sarafu float account is sent to the user (this amount can be based on a bonding curve)


### Variables for excahnge rates ###
 - float_CIC_Supply = the total supply of the CIC on the float account
 - float_CIC_reserve = the total supply of Mpesa in the float account
 - trr = the connector weight on blockchain converter between supply and reserve (assuming 50%)
 - CIC_excahnge_amount = the amount of CIC the user has sent to exhange for Mpesa
 - Mpesa_excahnge_amount = the amount of MPesa the user has sent to exhange for CIC
 - Rate: CIC -> KSH
 - Rate: KSH -> CIC
 - Fees: Sarafu to Mpesa FeeCC2M = 2%
 - Fees: Sarafu to Mpesa FeeM2CC = 2%
 - Alert on float balance and excahnge rate levels
 
### Incoming CIC Rules - MPESA out ###
 The amount of Mpesa given should be calculated as follows:
 - CIC_excahnge_amount = the amount of CIC the user wants to excahnge

Quotation: The Reserve (Mpesa) to be pulled out (reported):

Assuming a (trr=50%) connector weight:  return (float_CIC_reserve x CIC_exchange_amount ) / (float_CIC_reserve + CIC_excahnge_amount)
- reserveOut_Mpesa_reported = the the above amount of Mpesa that would be pulled from the Mpesa float account minus any fees (FeeM2CC)

Note that this should be the minimum amount the user will get - if they confirm (not the max)

After the user confirms the quotation - and executes the conversion: they should get out a minimum of reserveOut_Mpesa_reported

### Outgoing Sarafu Rules - MPESA In ###
inverse of the above
 
### Exchange Rate ###

When dislaying the SPOT price or excahnge rate to a user we should use the following formula:
> Exchange Price = float_CIC_reserve / (float_CIC_supply * trr) -> this is pulled from blockchain and should be sent back as a SMS


### USSD Interface ###

- Option #4 Exchange
   - #1 Get Exchange Rate
   - #2 Confirm Exchange

- User chooses Exchange Menu option #1 Get Exchange Rate - then chooses the direction of the exchange
   - #1 [CIC] -> Mpesa
      - Enter the amount of [CIC] you wish to redeem (limited to their balance)
   - #2 Mpesa -> [CIC]
      - Enter the amount of Mpesa you wish to contribute
   - Enter pin to confirm 
   - You will recieve a SMS shortly
       - perhpas for MVP we can just spit this back out on USSD. 
       - SMS: You will receive at least 800 KSH for 1000 CIC. This quotation is stored for the user but only valid for a period of time (1 hour?)
      
- User chooses Exchange Menu option #2 Confirm Exchange - if they don't have an active quotation they must goto option #1 first.
   
If the quotation is for CIC->Mpesa then: 
   - Show the last quotation message
   - Enter your pin to confirm
   - You will recieve a SMS shortly

If the quotation is for Mpesa-> then: 
   - Show the last quotation. message - which includes instruction for sending on paybill
   






### Credit Clearing ###
Weekly or after alterts we will rebalance our xDAI and Mpesa float accounts.

As our float account gets more and more full of xDAI (and mpesa float shrinks) - we will convert the xDAI to Mpesa
via xdai.io to get ETH - then using localcryptos or BitCoinSuisse to get Kenyan Shilllings / Mpesa (note there are MANY fees along this path)

If we get a large amount of Mpesa and want to convert to xDAI -> CIC ... then we send the Mpesa eitheir to localCryptos or bitcoinSwuiss to get ETH
then we use xdai.io to convert that ETH to xDAI ... then we add that to our xDAI float


### Security ###
 - Balance alerts: On both Mpesa and CIC balances: The paybill from which Sarafu is sent needs to send a sms and email alert when low 
 - Failure alerts: If the API session fails an sms and email alert should be sent
 - Reject on not enough Mpesa balance: If there is not enough Mpesa (on incoming Sarafu) the transaction should be rejected and an email sent to admin along with a SMS to admin and user (Sarafu should not be taken from user). (message should be visible also on mgmt platform txn view and user view and admin view).
 - Reject on not enough Sarafu balance: If there is not enough Sarafu (on incoming Mpesa) the transaction should be rejected and an email sent to admin along with a SMS to admin and user (Mpesa should not be taken from user). (message should be visible also on mgmt platform txn view and user view and admin view).

