# API EXTENSIONS FOR VIEWING NOTIFICATION DATABASE


## NOMENCLATURE

Values enclosed with `<>` are _required_.

Values enclosed with `[]` means _optional_.

No enclosure means _literal_.

## ADDED METHODS

Endpoints _added_ are found under /api/ext/

### `/api/ext/sms/` `/api/ext/sms/user/<user_id>/` `/api/ext/sms/<phone_number>/`

#### **GET**

Returns a list of latest sms notifications sent by the platform.

If `user_id` or `phone_number` is given, only entries for the affected user will be returned.

Accepts one query string:

```
	limit: [uint] - maximum items to return (default: 100)
```

Response payload:

```
[
	{
		datetime: <string>,
		number: <string>,
		message: <string>,	
	},
	...
]
```
