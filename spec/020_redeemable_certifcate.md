# Redeemable Certificate Contract spec
A generic redeemable certification contract that records certification information as well as forms of evidence and validations which  can be interacted with by redeemers that seek to reward the reciepent for the certificate. In effect this certificate can act as it's own ledgar - and used by marketplace applications that connect rewarders with certificate holders.

# Example 
After completing an online course the course creator issues a Certificate to the student. 
The student can accept the certificate by signing it. The student can also delete it. 
The details of the cert can include the course topics, as well as any txhashes or other data references.
A optional value in some currency can be placed on the certificate – this can be based on the duration of the course, cost of internet and so on.
Anyone can redeem the Cert. Say a business offered the receiver a voucher for their store with a stated value in come currency. If the receiver does an AcceptRedemption the value of the redemption will be removed from the AmountNotRecieved. And future redeemers can see what value is remaining on the Cert.

# Usages
* Personal Data: ex. Certifying that the recipent is a mother.
* Enviromental Data: ex. Certifying that the recipent is a farmer who is mulching their land or using regnerative practices. Soil quality testing etc.
* Transactional Data: ex. Certifying that the recipent is paying for school fees. The school could be a validator. If using CIC the transaction hashes themselves could add evidence.

# Schema
1. Issuer: <wallet address>
1. Issued to: <wallet address>
1. Acceptance: <signature of receiver>
1. Date Issued: 
1. Details: []
   1. Info: {description:’’, duration:’’,topics:’’, quality:’’}
   1. Location: {GPS, Country, Area}
   1. Transactions: []
        1. Description of data/transactions
        1. Hashes: []
   1. Suggested Value:
        1. Currency Name
        1. Currency <contract address>
        1. Suggested Value (amount of currency specified)
1. Validations: [] *appendable*
   1. Validated by
        1. Name
        1. Website
        1. Wallet address
1. Redemptions: [] *appendable*
   1. Redemption
        1. Acceptance status
        1. Name of Redemer
        1. Website
        1. Wallet address
        1. Type: Product, Voucher, Cash, Service
        1. Description: 
        1. Currency
        1. Value

# Contract Functions:
    * Issue: Anyone can issue a Cert (this specifies all the informaion and the recipient) - note there is no transfer function. Once issued to the reciever they can no longer send it to anyone else.
    * AcceptCert: The receiver can choose to accept the Certificate 
    * Destroy: The receiver can delete – remove data and send to 0x000
    * Validate: Anyone can validate a Cert
    * Redeem: Anyone can redeem the cert
    * AcceptRedemption: The receiver can accept a redemption. 

# Marketplace:

A market place application should be able to read these certificates and support redemption. 
Certificates can be aggregated and sorted by type, area, gender and so on. 
Recipients can go here to choose to delete their certs, share them with others or approve a redemption

# Risks & Questions
* Someone may potentially recieve many bogus certificates 
   * these should not be 'accepted' by the receiver and can also be Destroyed.
   * Validators should also have certifications 
* Personal Identifying Information written into the Cert is avalible to anyone that can read the Cert.
   * The reciever can have the option to encrypt the data or parts of the data/details
   * Zero knowlege proofs... tbt


