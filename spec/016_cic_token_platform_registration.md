# CIC TOKEN PLATFORM REGISTRATION

This document describes the components and steps involved to announce a token registration to the network.


## RATIONALE

Although registering a new token on the Bancor contracts will emit events upon which CIC client code can act upon, the information available is limited to the token itself and the exchange only. Therefore, a way to specify additional metadata for the entity that should be associated with the token must be provided.


## OVERVIEW

The system consists of two smart contracts and a metadata schema. The schema describes the actual metadata, and the contracts provide the announcement of the token metadata availability, and a mechanism to locate where the metadata can be retrieved from.

A separate, more formal document will be written as a request for comment for decentralized metadata dissemination for institutionally identifiable information.


### CONTRACTS

The `DecentralizedStorageResolver` contract facilitates multiplexing of data resource locations across web2 and web3 networks. It is a separately maintained project with its own documentation. Its utility here is to let an entity specify one or more locations where the metadata for the registration can be retrieved.

The `CICRegistration` contract announces the availability of metadata for a given token. In essence it is a simple chain of mappings (names do not match contract properties):

```
{
	'token_address': {
		'token_announcer_address': {
			'resolver_address': <address>,
			'resolver_type': <bytes32>,
			'resolver_chain': <bytes32>,
		}
	}
}

```

The `resolver_type` is a sha256 hash of a custom resolver id. The only three recognized values at current time are:
* `sha256(DecentralizedStorageResolver)`, the resolver mentioned above
* `sha256(ENS1)` - first generation ENS
* `sha256(ENS2)` - second generation ENS , which refers to the aforementioned contract. 

`resolver_chain` is a custom chain identifier string, which identifies which and what type of decentralized consensus resource holds the resolver record. It has the following format:

```
<platform>:<version>:<network>:<chain>
```

Example values are:

* `ethereum:1:bloxberg:8995`
* `ethereum:1:main:1`

### SCHEMA

The schema defines how the metadata must be formatted. The data type is json, and has the following layout:

(TODO: write in official schema spec language instead if exists)

```
{
	"schema_version": 1,
	"network": "bancor",
	"token": "<hex>",
	"identities": {
		"default": "<rfc-2426, must be unencrypted>",
		"[recipient_hint]": "<rfc-2426, optionally encrypted>",
		...
	},
}

[] = optional value
<hex> = 0x-prefixed hexadecimal value
[recipient_hint] = (optional) arbitrary value that can be announced off-band to specific consumers, in order to instruct how to decrypt the values. Cannot be "default"
<entity> = arbitrary string specifying an organisation or person to associate with the token.
```

The actual organisation and person detail records are given in RFC-6350 vcard format. The **MINIMUM** amount of data the default entity can contain is:

```
BEGIN:VCARD
ORG: <organisation_name_to_register>
X-COUNTRY-CODE: <ISO 639-1 country_ode>
TZ:<tzdata>
END:VCARD
```

### SEMPO PLATFORM INTEGRATION

To register all necessary information for a new token with associated organisation and exchange, we require the following items of information:

- bancor registry address
- token address
- organisation name
- organisation country
- organisation timezone
- admin email

A new blockchain listener component is needed that monitors the `CICRegistration` contract for `TokenRegistration` events. The first iteration will have these limitations:

* Will only work with `DecentralizedStorageResolver`.
* Expect the record in the `DecentralizedStorageResolver` to already be available at time of announcement.
* Only use the default identity. 
* Only read the minimum of entries required (see below). Any additional fields in the VCARD will be ignored for now.
* No encryption supported.

The minimum content of the VCARD for Sempo platform token metadata registration is:

```
BEGIN:VCARD
ORG: Acme Inc.
X-COUNTRY-CODE: ke
TZ:Africa/Nairobi
EMAIL: admin@acme.org
END:VCARD
```

### RESOURCES

* [CICRegistration contract](https://gitlab.com/grassrootseconomics/cic-contracts/-/blob/master/cic-registration/contracts/CICRegistration.sol)
* [DecentralizedStorageResolver contract](https://gitlab.com/nolash/resolvemux/-/blob/master/Resolver.sol)
* [RFC 6350](https://tools.ietf.org/html/rfc6350)
