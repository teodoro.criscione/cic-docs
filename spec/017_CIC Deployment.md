# CIC TOKEN TOOLING

This document is WIP.

## RATIONALE

Simplify execution of network deployment, token deployment.



The tooling should faciliate the following functions:

* Bootstrap network
* Deploy new CIC token/converter pair
* List tokens in network
* Query registry for contracts in network
* Query CIC token supply
* Query converter stats (weight, supply, fee)
* Mint additional tokens
* Burn tokens

<!--all: simplify code (use web3 arg builders rather than manual ones)
1. 1+2: connect to wallet using jsonrpc
1. 2: enable initial tx to deposit reserve and first mint
1. 2+3: connect with my metadata registry contract (deduplication of token symbols)
-->

