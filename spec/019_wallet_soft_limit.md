# Title

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Firstname Lastname <email> (url)
* Date: YYYY.MM.DD
* Version: 1
* Status: Pre-draft

## Rationale
<!--
Why are you proposing this?
-->
We want to remind users to not accept more Sarafu than they are willing to. Hence we want to give them the choice to set their own soft limit of how many of a token (i.e. Sarafu) that they have in their wallet.


## Before 
<!--
What is the current state of the described topic?
-->
no ability to set a max balance limit on USSD or web

## After
<!--
How will things be different after this has been done?
-->
ability to set a max balance limit on USSD or web (which can be adjusted)

## Implementation
<!--
Here is the description of how these changes should be implemented.
Please use subheadings to improve readability.
Some suggestions:

### Workflow

### Variables

### Interface
-->
Users are given a default inital limit of 1000 (after which there will be an error when trying to add more tokens). 
The user can set this to any nonzero integer - as a profile setting



## Testing
<!--
Please describe what test vectors that are required for this implementation
-->

## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
