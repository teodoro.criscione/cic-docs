# CIC Wallet Infrastructure 

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Gustav Friis
* Date: 2020.07.28
* Version: 0.1
* Status: Pre-draft



## Purpose

Build a non-custodial wallet infrastructure designed to enable Kenyan end-users to use xDAI and / or Bloxberg blockchain based Community Inclusion Currencies (CICs) from a smartphone and / or desktop.

## Target group

Existing or new Kenyan CIC end-users, who are individuals or small businesses using CICs to pay for goods and services. From the user-interviews with B4H we’ve found that these users are certainly not web3 savvy and to a varying degree are web2 savvy either. He/she just wants to interact with his/her wallet funds (displaying balance, paying & receiving) in the community currency (which value let's remember is equivalent to Kenyan Shilling).

## User-stories



- Get directed to web wallet via URL

- Signing up

- Choosing a reference currency

- Displaying balance

- Send payment to known contact

- Send payment to unknown receiver (referral link)

- Initiate payment by scanning QR code

- Receive payment via QR code

- Receive payment via link

- Claim payment link

- Payment notification

- Transaction details

- Transaction history overview

- Cash in and out from CICs to M-Pesa 

**Settings including**

- Backup wallet
- Change base currency
- Support
- Social media 
- Sign out
- Share with friends 

## Technical implementation


### Wallet creation

Using BIP39 HD and secp256k1

### Wallet authentication and recovery

Based on an open-source fork of Portis https://assets.portis.io/white-paper/latest.pdf, but instead of relying only on email as a string it can be any identifier which Grassroots Economica already has in place for its users.

The current PoC implementation of this can be found here:

https://github.com/multiplycharity/multiply-monorepo/tree/master/packages/sdk
https://github.com/multiplycharity/multiply-monorepo/blob/master/packages/server/src/controllers/accounts.js

From the Portis Whitepaper allowing SMS / Email / Password authenticated and recovery:

“Portis lets users create an encryption key on their Client, so once they generate blockchain wallets on their devices, they will be able to encrypt them using said encryption key, and store the encrypted wallets on the Portis servers. All cryptographic keys are generated and managed by the user on their devices, and all encryption is done locally in the Client. Portis servers are never in the position of learning your cryptographic keys. When the already encrypted data travels between the user’s device and our servers, it is encrypted and authenticated by TLS. All of the user’s sensitive data is encrypted when they create their account using 64 random bytes generated on the Client, protected using a password that they select. Nobody on earth knows this password besides them as it never leaves the Client. Using a KDF algorithm, a Backup Recovery Phrase is derived from the password, to offer users a means of resetting their password in case they forget it.”


### Interface with the existing Grassroots Economics Platform


Existing USSD users
Gas fees on xDAI
AfricasTalking API 


### Claimable transactions 


Using the open-source Linkdrop SDK

	
## Tech stack


Expo
React Native
MongoDB
Node.js


## Portability

Portability into native mobile apps and desktop apps following this pattern https://www.youtube.com/watch?v=ykBxY01j_rA



## Future developments

Migrate to Gnosis Safe smart-contract wallet
User facing decentralized exchange
User facing on-the ground marketplace
Native implementation of P2P platforms like Mylocalcrypto




