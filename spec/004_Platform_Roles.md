# Public View Only

1. View public data only and no ability to edit

# Private View Only 

1. View private data and no ability to edit

Private data inlcudes, names, phone numbers, location

# Enroller

1. See private data
1. Add users
1. Edit user fields
1. Pin reset (in future this will need admin approval)
1. Initial Disbursement only (1 time per user with a max of 350)
1. Disbursement (besides initial) and reclamation (with approval of Admin)
1. Reversal (with approval of Admin)

# Admin

1. See private data
1. Add users
1. Edit user fields
1. Pin reset
1. Disbursement and reclamation and Reversal (without approval)
1. Give approval to Enrollers

# Super Admin

1. Assign the roles (Super Admin, Enroller, View Only)
1. See private data
1. Add users
1. Edit user fields
1. Pin reset
1. Disbursement and reclamation (without approval)
1. Give approval to Enrollers

